---
title: Membership
type: join
layout: single
intro:
  subtitle: Welcome to the era of Open Computing!
  intro:
    - The OpenPOWER Foundation is a non-profit organization supporting the free and open RISC instruction set architecture and extensions. We enable open community collaboration, technology advancements in the OpenPOWER ecosystem, and visibility of OpenPOWER successes.
    - Join us and see how open technical collaboration along with the support of many OpenPOWER programs can help drive your business forward.
  teaser:
     title: Why Join?
  options:
    - Accelerate technical development of your own POWER based products
    - Contribute technical resource, best practices, and code to help guide and influence OpenPOWER deliverables
    - Increase visibility as OpenPOWER amplifies member success across the industry
    - Build a partner network as an active member within the OpenPOWER community
    - Showcase your OpenPOWER products, services, training, and resources on OpenPOWER Ready
become-member:
  title: Become a Member
  options:
   - title: Individual membership
     text: Individual personal membership
     cta_url: 'https://enrollment.lfx.linuxfoundation.org/?individual&project=openpowerfoundation'
     cta_text: Join as an Individual Member
     image: individualmember.png
   - title: Corporate membership
     text: Corporate company membership
     cta_url: 'https://enrollment.lfx.linuxfoundation.org/?project=openpowerfoundation'
     cta_text: Join as a Corporate Member
     image:  corporateboard.png
date: 2022-01-03
draft: false
---
