---
title: 'Membership Benefits'
date: 2021-07-17
draft: false
---

Anyone may participate in OpenPOWER.
Membership levels are designed for those who are investing in growing and enhancing the OpenPOWER community and its proliferation within the industry.  

| OpenPOWER Foundation Membership Matrix                     | Associate | ISV     | Silver  | Gold    | Platinum |
|------------------------------------------------------------|-----------|---------|---------|---------|----------|
| Working Group Participation                                | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Marketing Committee Participation                          | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Access to Specifications Approved by the Board             | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Access to Draft Specifications in development              | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Petition TSC to create a new workgroup or project          | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Exhibit opportunities as OPF-hosted and exhibiting events  | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Participate in OpenPOWER Ambassador Program                | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Speaker Opportunities at OPF-hosted events                 | {{<v>}}   | {{<x>}} | 1+      | 2+      | 2+       |
| Keynote Opportunities at OPF-hosted events                 | {{<x>}}   | {{<x>}} | {{<x>}} | {{<x>}} | {{<v>}}  |
| Registration/exibit at OPF events                          | {{<x>}}   | {{<x>}} | 5%      | 10%     | 20%      |
| Member logo featured on the website                        | {{<v>}}   | {{<v>}} | {{<v>}} | {{<v>}} | {{<v>}}  |
| Member logo featured on the website homepage               | {{<x>}}   | {{<x>}} | {{<x>}} | {{<v>}} | {{<v>}}  |
| Member logo featured on event signage                      | {{<x>}}   | {{<x>}} | {{<x>}} | {{<v>}} | {{<v>}}  |
| Member profile featured on website spotlight               | {{<x>}}   | {{<x>}} | {{<x>}} | {{<v>}} | {{<v>}}  |
| Member name inclusion in Press Releases                    | {{<x>}}   | {{<x>}} | {{<x>}} | 1/year  | 2/year   |
| Final Approval of New Projects                             | <span>Board membership required</span>             |
| Final Approval and Voting of IP and Specifications         | <span>Board membership required</span>             |
| Manage the Future Direction of the Organization            | <span>Board membership required</span>             |

<span style="font-size: 10px;">
The information in this chart is subject to change by OpenPOWER Foundation without notice.
The Bylaws of OpenPOWER Foundation set forth the terms of membership and shall take precedence over any other information regarding membership.  
</span>
