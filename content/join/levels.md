---
title: Membership Levels
date: 2022-01-03
draft: false
---

The OpenPOWER Foundation is a 501c6 not-for-profit entity with a Board of Directors and a Technical Steering Committee.

- Membership levels provide either a default Board of Director position (Platinum)
  or an opportunity to be elected to the Board (Gold, Silver, and Associate/Academic members).
  The Bylaws detail additional governance by the Board including maximum seats, terms, etc.
- The Technical Steering Committee is formed from the Work Group Leads from the core projects and one representative designated by each Platinum member.

OpenPOWER offers a tiered membership of Platinum, Gold, Silver, and Associate/Academic memberships

- Annual fee and dedicated full-time equivalent (FTEs) – verification of committed number of FTEs on honor system
- Contributors, committers, Work Group leads and project leads influence Technical Steering Committee
- Associate/Academic level is not available to corporations


| Membership Level | Annual Fee | FTEs | Technical Steering Committee                  | Board/Voting Position                          |
|------------------|------------|------|-----------------------------------------------|------------------------------------------------|
| Platinum         | $100k      | 10 | One seat per member not otherwise represented | Includes Board position, includes TSC position |
| Gold             | $60k       | 3  | May be on TSC if Work Group lead              | Gold members may elect up to one BOD member per three Gold members |
| Silver           | $20k<br>$5k if <300 employees<br>$0 if < 300 employees and an Independent Software Vendor (ISV) or for Open Source Community Projects 										| 0    | May be on TSC if Work Group lead              | One board seat elected by all Silver members   |
| Associate and Academic | $0  | 0 | May be on TSC if Work Group lead | May be elected to one community observer, non-voting Board seat |

<span style="font-size: 10px;">Fee in US dollars</span>
