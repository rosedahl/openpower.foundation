---
title: Linux on POWER Architecture Reference
group: systemsoftware
tags:
  - software
  - linux
  - operatingsystem
  - distribution
  - architecture
date: 2020-07-02
draft: false
---

The purpose of this document is to detail a stable platform architecture to be used by platforms defined by the POWER ISA Specification.  

This architecture specification provides a comprehensive computer system platform-to-software interface definition,
combined with minimum system requirements, that enables the development of and software porting to
a range of compatible industry-standard computer systems from workstations through servers.
These systems are based on the requirements defined in the Power Instruction Set Architecture (ISA).
The definition supports the development of both uni-processor and multi-processor system implementations.  

This document is a Standard Track, Work Group Specification work product owned by the System Software Workgroup and
handled in compliance with the requirements outlined in the OpenPOWER Foundation Work Group (WG) Process document.
It was created using the Master Template Guide version 1.0.0.
Comments, questions, etc. can be submitted to the public mailing list for this document at syssw-linux_architecture_ref@mailinglist.openpowerfoundation.org.
