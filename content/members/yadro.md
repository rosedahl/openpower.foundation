---
title: "YADRO"
image: "yadro.png"
level: platinum
country: Russia
joined: 2015
link: "https://www.yadro.com/"
date: 2021-03-02
draft: false
---

YADRO is the absolute leader on the market of enterprise storage systems and holds a leading position in the high-performance server segment in Russia.
A domestic vendor has never before placed this high in the segments of the Russian market historically dominated by the large global vendors.
