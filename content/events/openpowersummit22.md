---
title: "OpenPOWER Summit EU 2022"
eventdates:
  from: 2021-10-31
eventplace: "Lyon, France"
eventcalendar: Google Calendar ICS
eventtime: 10:00am 6:00pm
image: thumb-7.jpg
link: '#'
date: 2021-10-28
draft: true
---
