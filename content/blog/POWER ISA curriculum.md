---
title: Virginia Tech opens up MIPS and POWER based Computer Architecture Curriculum
categories:
  - blogs
tags:
  - openpower
  - openpower-foundation
  - linux-foundation
  - open-source
  - Virginia Tech
  - Curriculum
  - Computer Architecture
date: 2022-09-14
draft: true
---

Today, we are pleased to announce that MIPS and POWER based computer architecture curriculum developed by Dr. Wu Feng of Virginia Tech has been released publicly as open source.

https://github.com/w-feng/CompArch-MIPS-POWER