---
title: 'High-Performance-Computing SIG'
wgtype: sig
image: thumb-3.jpg
chair:
  - allancantle
members:
  - allancantle
  - ibm
  - innoboost
  - yadro
  - vantosh
date: 2021-03-15
draft: false
---
