---
title: 'System Software TWG'
wgtype: twg
image: thumb-3.jpg
chair:
  - michalzygowski
members:
  - ibm
  - vantosh
  - yadro
  - 3mdeb
  - stevenmunroe
participation: 'Members'
git: https://git.openpower.foundation/systemsoftware
discussion: https://discuss.openpower.foundation/c/twg/syssoftware/19
meetingminutes: https://meetingminutes.openpower.foundation/systemsoftware/
calendar:
  web: https://discuss.openpower.foundation/c/twg/syssoftware/19/l/calendar
  ics: webcal://discuss.openpower.foundation/c/twg/syssoftware/l/calendar.ics
chat:
  mattermost: https://chat.openpower.foundation/opf/channels/systemsoftware
files: https://files.openpower.foundation/f/475
kanban: https://kanban.openpower.foundation/b/7ZvTJoH3sFbTF25Gk/system-software
date: 2021-07-27
draft: false
---
