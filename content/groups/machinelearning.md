---
title: 'Machine Learning & AI SIG'
wgtype: sig
image: thumb-2.jpg
chair:
  - lionelclavien
  - kipwarner
members:
  - innoboost
  - google
  - ibm
  - vantosh
  - yadro
  - cartesiantheatre
date: 2021-03-15
draft: false
---
