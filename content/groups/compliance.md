---
title: 'Compliance TWG'
wgtype: twg
image: thumb-2.jpg
chair:
  - sandywoodward
members:
  - ibm
  - yadro
  - vantosh
date: 2021-03-15
draft: false
---

The OpenPOWER Compliance Work Group will define OpenPOWER key interfaces that need to be compliant in a standard specification of compliance
and define how to measure and document compliance pre-silicon and post-silicon.

Subcommittees for each set of related key interfaces will be formed to address reference test harnesses and reference test suites.
This will allow for focused effort by the experts and interested members of the related key interfaces.
The importance of this Work Group is to be able to maintain software and hardware interoperability through following the compliance specifications.
