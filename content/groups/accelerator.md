---
title: 'Accelerator TWG'
wgtype: twg
image: thumb-5.jpg
chair:
  - curtwollbrink
members:
  - ibm
  - vantosh
  - yadro
  - allancantle
participation: 'Member'
git: https://git.openpower.foundation/accelerator/
discussion: https://discuss.openpower.foundation/c/twg/accelerator/
meetingminutes: https://meetingminutes.openpower.foundation/accelerator/
calendar:
  web: https://discuss.openpower.foundation/c/twg/accelerator/31/l/calendar
  ics: webcal://discuss.openpower.foundation/c/twg/accelerator/l/calendar.ics
#chat:
  mattermost: https://chat.openpower.foundation/opf/channels/accelerator
  #slack:
irc: '#openpower-accelerator on irc.libera.chat'
#files:
#kanban:
date: 2021-12-28
draft: false
---

The Accelerator Technical Working Group will be a persistent, standing group that defines, documents, manages, and maintains standards
which define the interfaces between the processor and accelerator devices and their associated development tools.
This includes hardware, firmware and any other software require for accelerators to operate within OpenPOWER compliant systems.
It will also identify future requirements and propose innovations to the interfaces to enable advances in accelerator capabilities and
work with other working groups as appropriate.
